package com.joasvpereira.cocus.codechallange.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.joasvpereira.cocus.codechallange.db.entities.UserEntry

/**
 * Created by Joás V. Pereira
 * on 06 Feb. 2019.
 */

@Dao
interface UserDao{
    @Query("select * from users" + " ORDER BY time DESC")
    fun getUsersById(): List<UserEntry>

    @Query("select * from users" + " ORDER BY time ASC")
    fun getUserByRank(): List<UserEntry>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(userEntry: UserEntry)
}
