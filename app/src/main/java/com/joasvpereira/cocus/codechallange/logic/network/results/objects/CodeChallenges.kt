package com.joasvpereira.cocus.codechallange.logic.network.results.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Joás V. Pereira
 * on 27 Jan. 2019.
 */

data class CodeChallenges(
        @Expose
        @SerializedName("totalCompleted")
        val totalCompleted: Int = 0,
        @Expose
        @SerializedName("totalAuthored")
        val totalAuthored: Int = 0
)