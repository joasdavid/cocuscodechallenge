package com.joasvpereira.cocus.codechallange.logic.repository

/**
 * Created by Joás V. Pereira
 * on 06 Feb. 2019.
 */
interface CallbackInterface<T>{
    fun onResult(data:RepositoryResult<T>)
}