package com.joasvpereira.cocus.codechallange.ui.user

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.joasvpereira.cocus.codechallange.R
import com.joasvpereira.cocus.codechallange.constants.ShareDataConstants
import com.joasvpereira.cocus.codechallange.logic.network.results.UserResult
import com.joasvpereira.cocus.codechallange.ui.BaseFragment
import com.joasvpereira.cocus.codechallange.ui.adapters.UsersAdapter
import com.joasvpereira.cocus.codechallange.ui.challenge.ChallengesContainerFragment
import com.joasvpereira.cocus.codechallange.viewmodes.user.SearchUserViewModel
import kotlinx.android.synthetic.main.activity_base.*
import kotlinx.android.synthetic.main.search_user_fragment.*
import javax.inject.Inject

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
class SearchUserFragment : BaseFragment(), UsersAdapter.ItemSelectedInterface {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    lateinit var viewModel: SearchUserViewModel

    internal var adapter: UsersAdapter = UsersAdapter(this)
    internal var isRankedSearch: Boolean = false

    override fun pageTitle(): String {
        return resources.getString(R.string.search_user_fragment_title)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.search_user_fragment,
                container,
                false
        )

        getBaseActivity().appComponent.inject(this)
        viewModel = ViewModelProviders.of(this, factory).get(SearchUserViewModel::class.java)

        return view
    }

    override fun onResume() {
        super.onResume()
        initUi()
        initObserves()
    }

    fun initUi() {

        searchBt.setOnClickListener { searchBtClick() }

        searchByBt.setOnClickListener {
            isRankedSearch = !isRankedSearch
            if (isRankedSearch)
                searchByBt.text = resources.getString(R.string.searched_by_rank)
            else
                searchByBt.text = resources.getString(R.string.search_by_last_look_up)
            fetchList()
        }


        userListRv.layoutManager = LinearLayoutManager(context)
        //noinspection ConstantConditions
        userListRv.addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL))

        userListRv.adapter = adapter
    }

    private fun initObserves() {
        viewModel.loadingState().observe(this, Observer {
            loading(it)
        })

        viewModel.vmMessages().observe(this, Observer { it?.let { showMessages(it) } })

        viewModel.fetchListOfUsers(isRankedSearch).observe(this, Observer {
            it?.let {
                updateList(it)
            }
        })
    }

    private fun loading(boolean: Boolean?) {
        if (boolean == true)
            loadingView.visibility = View.VISIBLE
        else
            loadingView.visibility = View.GONE
    }

    private fun updateList(users: List<UserResult>) {
        adapter.userResultList = users
        searchByBt.isEnabled = users.size > 0
    }

    private fun fetchList() {
        viewModel.fetchListOfUsers(isRankedSearch)
    }

    private fun searchBtClick() {
        val searchText = searchEt.text
        if (!searchText.isEmpty()) {
            viewModel.searchUser(searchText.toString())
        }
    }


    override fun onItemClick(userResult: UserResult) {
        getBaseActivity().screensShareData.put(
                ShareDataConstants.USER,
                userResult.username
        )

        getBaseActivity().replaceFragment(
                getBaseActivity().baseFragmentContainer,
                ChallengesContainerFragment(),
                getBaseActivity().supportFragmentManager,
                true
        )
    }
}