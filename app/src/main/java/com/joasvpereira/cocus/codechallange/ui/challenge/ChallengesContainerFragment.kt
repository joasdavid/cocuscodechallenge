package com.joasvpereira.cocus.codechallange.ui.challenge

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.joasvpereira.cocus.codechallange.R
import com.joasvpereira.cocus.codechallange.ui.BaseFragment
import com.joasvpereira.cocus.codechallange.viewmodes.user.SearchUserViewModel
import kotlinx.android.synthetic.main.activity_base.*
import kotlinx.android.synthetic.main.challenges_container.*

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
class ChallengesContainerFragment : BaseFragment() {

    override fun pageTitle(): String {
        return ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.challenges_container,
                container,
                false
        )
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bottomNavigation.setOnNavigationItemSelectedListener(navigationOptions())
        bottomNavigation.selectedItemId = R.id.action_completed

        //getBaseActivity().appComponent.inject(this)
        //viewModel = ViewModelProviders.of(this, factory).get(SearchUserViewModel::class.java)
    }

    private fun navigationOptions(): BottomNavigationView.OnNavigationItemSelectedListener {
        return BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_completed ->
                    getBaseActivity().replaceFragment(
                            challengesFragmentContainer,
                            CompletedChallengesFragment(),
                            childFragmentManager,
                            false
                    )
            R.id.action_authored ->
            getBaseActivity().replaceFragment(
                    challengesFragmentContainer,
                    AuthoredChallengesFragment(),
                    childFragmentManager,
                    false
            )
            }

            true
        }
    }

}