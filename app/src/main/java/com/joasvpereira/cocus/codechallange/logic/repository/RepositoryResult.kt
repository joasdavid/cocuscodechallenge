package com.joasvpereira.cocus.codechallange.logic.repository

/**
 * Created by Joás V. Pereira
 * on 28 Jan. 2019.
 */
class RepositoryResult<T>(val isSuccessState: Boolean = false,
                          var dataResult: T? = null,
                          var message: String = "")