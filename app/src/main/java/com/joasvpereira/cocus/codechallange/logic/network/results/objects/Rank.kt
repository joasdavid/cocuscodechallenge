package com.joasvpereira.cocus.codechallange.logic.network.results.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
data class Rank(@Expose
                @SerializedName("color")
                val color: String = "",
                @Expose
                @SerializedName("name")
                val name: String = "",
                @Expose
                @SerializedName("id")
                val id: Int = 0)