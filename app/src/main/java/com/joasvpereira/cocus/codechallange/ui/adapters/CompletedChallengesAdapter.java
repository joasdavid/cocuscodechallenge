package com.joasvpereira.cocus.codechallange.ui.adapters;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.joasvpereira.cocus.codechallange.R;
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.CompletedChallenge;
import com.joasvpereira.cocus.codechallange.ui.adapters.holders.AdapterItemInterface;
import com.joasvpereira.cocus.codechallange.ui.adapters.holders.ChallengeViewHolder;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 21 Aug. 2018.
 */

public class CompletedChallengesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private final int VIEW_TYPE_ITEM = 0;
  private final int VIEW_TYPE_LOADING = 1;
  private final int VIEW_TYPE_END = 2;
  private OnLoadMoreListener onLoadMoreListener;
  private boolean isLoading;
  private List<CompletedChallenge> completedChallengesList;
  private int visibleThreshold = 100;
  private int lastVisibleItem, totalItemCount;
  private boolean isLastPageLoaded = false;
  private AdapterItemInterface itemInterface;

  public CompletedChallengesAdapter(RecyclerView recyclerView,
      @Nullable
          AdapterItemInterface itemInterface
  ) {
    this.itemInterface = itemInterface;
    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
        .getLayoutManager();
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        totalItemCount = linearLayoutManager.getItemCount();
        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
        if (!isLastPageLoaded && !isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
          completedChallengesList.add(null);
          notifyItemInserted(completedChallengesList.size() - 1);
          if (onLoadMoreListener != null) {
            onLoadMoreListener.onLoadMore();
          }
          isLoading = true;
        }
      }
    });
  }

  public void addCompletedChallenges(List<CompletedChallenge> completedChallengesList) {
    int size = getItemCount();
    if (this.completedChallengesList == null) {
      this.completedChallengesList = completedChallengesList;
    } else {
      this.completedChallengesList.remove(size - 1);
      isLoading = false;
      this.completedChallengesList.addAll(completedChallengesList);
    }
    notifyDataSetChanged();
  }

  public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
    this.onLoadMoreListener = mOnLoadMoreListener;
  }

  @Override
  public int getItemViewType(int position) {
    if (position == totalItemCount -1 && isLastPageLoaded) {
      return VIEW_TYPE_END;
    }
    return completedChallengesList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    if (viewType == VIEW_TYPE_ITEM) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.item_completed_challenges, parent, false);
      return new ChallengeViewHolder(view,itemInterface);
    } else if (viewType == VIEW_TYPE_LOADING) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.item_loading, parent, false);
      return new LoadingViewHolder(view);
    } else if (viewType == VIEW_TYPE_END) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.item_list_end, parent, false);
      return new EndOfListViewHolder(view);
    }
    return null;
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    if (holder instanceof ChallengeViewHolder) {
      CompletedChallenge challengesResult = completedChallengesList.get(position);
      ChallengeViewHolder challengeViewHolder = (ChallengeViewHolder) holder;
      challengeViewHolder.challengesNameTv.setText(challengesResult.getName());
    } else if (holder instanceof LoadingViewHolder) {
      LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
      loadingViewHolder.progressBar.setIndeterminate(true);
    }
  }

  @Override
  public int getItemCount() {
    return completedChallengesList == null ? 0 : completedChallengesList.size();
  }


  public void setLoaded() {
    isLoading = false;
  }


  public void notifyLastPageLoaded() {
    isLastPageLoaded = true;
    notifyItemChanged(getItemCount() -1);
  }

  public List<CompletedChallenge> getCompletedChallengesList() {
    return completedChallengesList;
  }

  private class LoadingViewHolder extends RecyclerView.ViewHolder {

    public ProgressBar progressBar;

    public LoadingViewHolder(View view) {
      super(view);
      progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
    }
  }

  public class EndOfListViewHolder extends ViewHolder {

    public EndOfListViewHolder(View view) {
      super(view);
    }
  }
}

