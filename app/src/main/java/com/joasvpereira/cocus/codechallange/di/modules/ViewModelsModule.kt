package com.joasvpereira.cocus.codechallange.di.modules

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.joasvpereira.cocus.codechallange.di.annotation.ViewModelKey
import com.joasvpereira.cocus.codechallange.viewmodes.ViewModelFactory
import com.joasvpereira.cocus.codechallange.viewmodes.challenge.authored.AuthoredChallengesViewModel
import com.joasvpereira.cocus.codechallange.viewmodes.challenge.completed.CompletedChallengesViewModel
import com.joasvpereira.cocus.codechallange.viewmodes.challenge.details.CodeChallengesViewModel
import com.joasvpereira.cocus.codechallange.viewmodes.user.SearchUserViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Joás V. Pereira
 * on 06 Feb. 2019.
 */
@Module
abstract class ViewModelsModule {
    @Binds
    @IntoMap
    @ViewModelKey(SearchUserViewModel::class) // PROVIDE YOUR OWN MODELS HERE
    internal abstract fun bindSearchUserViewModel(viewModel: SearchUserViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CompletedChallengesViewModel::class) // PROVIDE YOUR OWN MODELS HERE
    internal abstract fun bindCompletedChallengesViewModel(viewModel: CompletedChallengesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AuthoredChallengesViewModel::class) // PROVIDE YOUR OWN MODELS HERE
    internal abstract fun bindAuthoredChallengesViewModel(viewModel: AuthoredChallengesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CodeChallengesViewModel::class) // PROVIDE YOUR OWN MODELS HERE
    internal abstract fun bindCodeChallengesViewModel(viewModel: CodeChallengesViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}