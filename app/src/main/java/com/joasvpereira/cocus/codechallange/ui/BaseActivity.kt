package com.joasvpereira.cocus.codechallange.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.joasvpereira.cocus.codechallange.CodeChallangeAplication
import com.joasvpereira.cocus.codechallange.R
import com.joasvpereira.cocus.codechallange.di.componets.AppComponent
import com.joasvpereira.cocus.codechallange.ui.user.SearchUserFragment
import kotlinx.android.synthetic.main.activity_base.*
import java.util.*

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */

abstract class BaseActivity : AppCompatActivity() {

    var screensShareData = HashMap<String, Any>()

    val appComponent: AppComponent
        get() = (application as CodeChallangeAplication).appComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
    }

    fun replaceFragment(viewID: Int,
                        fragment: Fragment,
                        fragmentManager: FragmentManager,
                        isAddToBackStack: Boolean) {
        fragmentManager
                .beginTransaction()
                .replace(viewID, fragment)
                .addToBackStack(backStackTag(
                        isAddToBackStack,
                        fragment.toString()
                ))
                .commit()

        if (isAddToBackStack)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun replaceFragment(view: View, fragment: Fragment,
                        fragmentManager: FragmentManager,
                        isAddToBackStack: Boolean) {

        val transaction = fragmentManager.beginTransaction()

        if (isAddToBackStack) {
            transaction
                    .addToBackStack(backStackTag(
                            isAddToBackStack,
                            fragment.toString()
                    ))
        }

        transaction
                .replace(view.id, fragment)
                .commit()

        if (isAddToBackStack)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun backStackTag(isAddToBackStack: Boolean, tag: String): String? {
        if (isAddToBackStack)
            return tag
        else
            return null
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount <= 1) {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
        super.onBackPressed()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun openUrl(url:String){
        val link = Uri.parse(url)
        val myIntent = Intent(Intent.ACTION_VIEW, link)
        myIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(myIntent)
    }
}
