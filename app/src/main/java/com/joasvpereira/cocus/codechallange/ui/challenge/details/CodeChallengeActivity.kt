package com.joasvpereira.cocus.codechallange.ui.challenge.details

import android.os.Bundle
import com.joasvpereira.cocus.codechallange.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_base.*

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */

class CodeChallengeActivity : BaseActivity() {

    companion object {
        var CODE_CHALLENGE_CODE = "CODE_CHALLENGE_CODE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent.extras != null
                && intent.extras.containsKey(CODE_CHALLENGE_CODE)) {

            screensShareData.put(
                    CodeChallengeFragment.CODE_CHALLENGE_CODE,
                    intent.extras.get(CODE_CHALLENGE_CODE)
            )

            replaceFragment(baseFragmentContainer,
                    CodeChallengeFragment(),
                    supportFragmentManager,
                    false
            )
        } else {
            finish()
        }
    }

}
