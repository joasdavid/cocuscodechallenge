package com.joasvpereira.cocus.codechallange

import android.app.Application
import android.content.Context
import com.joasvpereira.cocus.codechallange.di.componets.AppComponent
import com.joasvpereira.cocus.codechallange.di.componets.DaggerAppComponent
import com.joasvpereira.cocus.codechallange.di.modules.AppModule
import com.joasvpereira.cocus.codechallange.di.modules.DataBaseModule
import com.joasvpereira.cocus.codechallange.di.modules.NetworkModule

/**
 * Created by Joás V. Pereira
 * on 06 Feb. 2019.
 */

class CodeChallangeAplication : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .networkModule(NetworkModule(
                        NetworkConfigs.END_POINT,
                        NetworkConfigs.CACHE_SIZE))
                .dataBaseModule(DataBaseModule(
                        DataBaseConfig.DB_NAME
                ))
                .build();
    }

    fun isConnected(): Boolean {
        try {
            val e = getSystemService(
                    Context.CONNECTIVITY_SERVICE) as android.net.ConnectivityManager
            val activeNetwork = e.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting
        } catch (e: Exception) {
            //void
        }
        return false
    }
}
