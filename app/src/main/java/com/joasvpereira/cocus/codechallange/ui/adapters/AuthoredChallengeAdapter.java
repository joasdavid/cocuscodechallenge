package com.joasvpereira.cocus.codechallange.ui.adapters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.joasvpereira.cocus.codechallange.R;
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.AuthoredChallenge;
import com.joasvpereira.cocus.codechallange.ui.adapters.holders.AdapterItemInterface;
import com.joasvpereira.cocus.codechallange.ui.adapters.holders.ChallengeViewHolder;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class AuthoredChallengeAdapter extends RecyclerView.Adapter<ChallengeViewHolder> {

  private List<AuthoredChallenge> list;
  private AdapterItemInterface adapterItemInterface;

  public AuthoredChallengeAdapter(
      @Nullable AdapterItemInterface adapterItemInterface) {
    this.adapterItemInterface = adapterItemInterface;
  }

  @NonNull
  @Override
  public ChallengeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_completed_challenges, parent, false);
    return new ChallengeViewHolder(view, adapterItemInterface);
  }

  @Override
  public void onBindViewHolder(@NonNull ChallengeViewHolder holder, int position) {
    AuthoredChallenge authoredChallenge = list.get(position);
    holder.challengesNameTv.setText(authoredChallenge.getName());
  }

  @Override
  public int getItemCount() {
    return (list == null)?0:list.size();
  }

  public List<AuthoredChallenge> getList() {
    return list;
  }

  public void addAuthoredChallenges(
      @Nullable List<AuthoredChallenge> it) {
    this.list = it;
  }
}
