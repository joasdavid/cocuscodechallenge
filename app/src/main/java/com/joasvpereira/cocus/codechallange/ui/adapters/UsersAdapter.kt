package com.joasvpereira.cocus.codechallange.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.joasvpereira.cocus.codechallange.R
import com.joasvpereira.cocus.codechallange.logic.network.results.UserResult
import kotlinx.android.synthetic.main.adapter_item_user.view.*

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */

class UsersAdapter(val anInterface: ItemSelectedInterface? = null) : RecyclerView.Adapter<UsersAdapter.UserViewHolder>() {

    var userResultList: List<UserResult> = emptyList()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): UserViewHolder {
        val view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.adapter_item_user, viewGroup, false)
        return UserViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserViewHolder, pos: Int) {
        val user: UserResult = userResultList[pos]

        holder.userName.text = user.username
        holder.rankValue.text = user.ranks?.overall?.name ?: ""

        holder.itemView.setOnClickListener { anInterface?.onItemClick(user) }
    }

    override fun getItemCount(): Int {
        return userResultList.size
    }

    class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val userName = itemView.userNameTv
        val rankValue = itemView.rankValueTv
        val language = itemView.scoreTV
        val languageValue = itemView.scoreValueTv
    }

    interface ItemSelectedInterface {
        fun onItemClick(userResult: UserResult)
    }
}