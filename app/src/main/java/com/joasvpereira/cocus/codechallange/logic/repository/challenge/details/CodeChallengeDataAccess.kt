package com.joasvpereira.cocus.codechallange.logic.repository.challenge.details

import com.joasvpereira.cocus.codechallange.logic.network.results.CodeChallengeResult
import com.joasvpereira.cocus.codechallange.logic.repository.CallbackInterface

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
interface CodeChallengeDataAccess{
    fun getCodeChallenges(code: String, callback: CallbackInterface<CodeChallengeResult>)
}