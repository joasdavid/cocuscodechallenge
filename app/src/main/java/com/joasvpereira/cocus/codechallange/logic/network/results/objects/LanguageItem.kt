package com.joasvpereira.cocus.codechallange.logic.network.results.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Joás V. Pereira
 * on 27 Jan. 2019.
 */

data class LanguageItem(
        @Expose
        @SerializedName("score")
        var score: Int = 0,
        @Expose
        @SerializedName("color")
        var color: String? = null,
        @Expose
        @SerializedName("name")
        var name: String? = null,
        @Expose
        @SerializedName("rank")
        var rank: Int = 0
)
