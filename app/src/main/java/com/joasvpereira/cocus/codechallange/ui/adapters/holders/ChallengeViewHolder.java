package com.joasvpereira.cocus.codechallange.ui.adapters.holders;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.joasvpereira.cocus.codechallange.R;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class ChallengeViewHolder extends ViewHolder {

  public TextView challengesNameTv;

  public ChallengeViewHolder(View view, @Nullable final AdapterItemInterface itemInterface) {
    super(view);
    challengesNameTv = itemView.findViewById(R.id.challengesNameTv);
    view.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        itemInterface.itemClicked(view,getAdapterPosition());
      }
    });
  }
}
