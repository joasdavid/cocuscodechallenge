package com.joasvpereira.cocus.codechallange.logic.repository.users

import com.joasvpereira.cocus.codechallange.logic.network.results.UserResult
import com.joasvpereira.cocus.codechallange.logic.repository.CallbackInterface

/**
 * Created by Joás V. Pereira
 * on 28 Jan. 2019.
 */
interface UserDataAccess {
    fun getUserByName(name: String, callback: CallbackInterface<UserResult>? = null)
    fun getUserList(isByRank: Boolean, callback: CallbackInterface<List<UserResult>>)
}