package com.joasvpereira.cocus.codechallange.db.entities

import android.annotation.SuppressLint
import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Joás V. Pereira
 * on 06 Feb. 2019.
 */

@Entity(tableName = "users")
data class UserEntry(
        @PrimaryKey
        @ColumnInfo(name = "entry_id")
        var uid:String = "John Doe",
        @ColumnInfo(name = "json")
        var userResultJson: String? = null,
        @ColumnInfo(name = "pos")
        var pos:Int = Integer.MAX_VALUE
){
    @ColumnInfo(name = "time")
    var time: String? = null
        @SuppressLint("SimpleDateFormat")
        get() {
            val dt1 = SimpleDateFormat("yyyyMMddhhmmss")
            return dt1.format(Calendar.getInstance().time)
        }

}
