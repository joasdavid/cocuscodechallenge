package com.joasvpereira.cocus.codechallange.logic.network.results

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.AuthoredChallenge

/**
 * Created by Joás V. Pereira
 * on 28 Jan. 2019.
 */
data class AuthoredChallengesResult(
        @Expose
        @SerializedName("data")
        internal var authoredChallenges: List<AuthoredChallenge>
)