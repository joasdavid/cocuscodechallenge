package com.joasvpereira.cocus.codechallange.logic.repository.challenge

import com.joasvpereira.cocus.codechallange.logic.network.results.AuthoredChallengesResult
import com.joasvpereira.cocus.codechallange.logic.repository.CallbackInterface

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
interface AuthoredChallengeDataAccess{
    fun getAuthoredChallenges(user: String, callback: CallbackInterface<AuthoredChallengesResult>)
}