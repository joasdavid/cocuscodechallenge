package com.joasvpereira.cocus.codechallange.logic.network.results.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Joás V. Pereira
 * on 28 Jan. 2019.
 */
data class AuthoredChallenge(
        @Expose
        @SerializedName("languages")
        var languages: List<String> = ArrayList(),
        @Expose
        @SerializedName("tags")
        var tags: List<String> = ArrayList(),
        @Expose
        @SerializedName("rankName")
        var rankName: String = "",
        @Expose
        @SerializedName("rank")
        var rank: Int = -1,
        @Expose
        @SerializedName("description")
        var description: String = "",
        @Expose
        @SerializedName("name")
        var name: String = "",
        @Expose
        @SerializedName("id")
        var id: String
)