package com.joasvpereira.cocus.codechallange.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import com.joasvpereira.cocus.codechallange.CodeChallangeAplication
import com.joasvpereira.cocus.codechallange.db.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Joás V. Pereira
 * on 06 Feb. 2019.
 */
@Module
class DataBaseModule(internal var databaseName:String){

    @Provides
    @Singleton
    fun providesRoomDataBase(application: CodeChallangeAplication): AppDatabase{
        return Room
                .databaseBuilder(
                        application,
                        AppDatabase::class.java,
                        databaseName)
                .allowMainThreadQueries()
                .build();
    }
}