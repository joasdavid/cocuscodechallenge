package com.joasvpereira.cocus.codechallange.logic.network.results.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
data class Unresolved(@Expose
                      @SerializedName("suggestions")
                      val suggestions: Int = 0,
                      @Expose
                      @SerializedName("issues")
                      val issues: Int = 0)