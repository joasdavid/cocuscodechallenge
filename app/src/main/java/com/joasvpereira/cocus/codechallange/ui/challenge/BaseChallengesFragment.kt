package com.joasvpereira.cocus.codechallange.ui.challenge

import android.content.Intent
import com.joasvpereira.cocus.codechallange.ui.BaseFragment
import com.joasvpereira.cocus.codechallange.ui.challenge.details.CodeChallengeActivity

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */

abstract class BaseChallengesFragment : BaseFragment(){

    fun openDetailsActivity(id:String){
        val intent = Intent(getBaseActivity(), CodeChallengeActivity::class.java)
        intent.putExtra(CodeChallengeActivity.CODE_CHALLENGE_CODE,id)
        getBaseActivity().startActivity(intent)
    }
}
