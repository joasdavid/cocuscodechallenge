package com.joasvpereira.cocus.codechallange.logic.network.results.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Joás V. Pereira
 * on 28 Jan. 2019.
 */
data class CompletedChallenge(
        @Expose
        @SerializedName("completedLanguages")
        var completedLanguages: List<String> = ArrayList(),
        @Expose
        @SerializedName("completedAt")
        var completedAt: String,
        @Expose
        @SerializedName("slug")
        var slug: String,
        @Expose
        @SerializedName("name")
        var name: String,
        @Expose
        @SerializedName("id")
        var id: String
)