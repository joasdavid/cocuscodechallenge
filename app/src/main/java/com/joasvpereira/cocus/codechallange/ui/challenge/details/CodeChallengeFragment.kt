package com.joasvpereira.cocus.codechallange.ui.challenge.details

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.joasvpereira.cocus.codechallange.R
import com.joasvpereira.cocus.codechallange.ui.BaseFragment
import com.joasvpereira.cocus.codechallange.viewmodes.challenge.details.CodeChallengesViewModel
import kotlinx.android.synthetic.main.code_challenge_fragment.*
import javax.inject.Inject

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
class CodeChallengeFragment : BaseFragment() {

    companion object {
        var CODE_CHALLENGE_CODE = "CODE_CHALLENGE_CODE"
    }

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    lateinit var viewModel: CodeChallengesViewModel
    lateinit var code: String

    override fun pageTitle(): String {
        return resources.getString(R.string.Code_challenge_fragment_title)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.code_challenge_fragment,
                container,
                false
        )
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        code = getBaseActivity().screensShareData.get(CODE_CHALLENGE_CODE) as String? ?: ""

        getBaseActivity().appComponent.inject(this)
        viewModel = ViewModelProviders.of(this, factory).get(CodeChallengesViewModel::class.java)

        initObserves()
    }

    fun initUI() {
    }

    fun initObserves() {
        viewModel.loadingState().observe(this, Observer {
            it.let { loading(it) }
        })

        viewModel.vmMessages().observe(this, Observer {
            it?.let {
                showMessages(it)
            }
        })

        viewModel.codeChallengeDetails(code).observe(this, Observer {
            nameTv.text = it?.name
            categoryValueTv.text = it?.category
            descriptionTv.text = it?.description
            val link = it?.url
            linkBt.setOnClickListener { getBaseActivity().openUrl(link!!) }
        })
    }

    private fun loading(boolean: Boolean?) {
        if (boolean == true)
            loadingView.visibility = View.VISIBLE
        else
            loadingView.visibility = View.GONE
    }

}