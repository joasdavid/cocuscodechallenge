package com.joasvpereira.cocus.codechallange.di.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.joasvpereira.cocus.codechallange.CodeChallangeAplication
import com.joasvpereira.cocus.codechallange.logic.network.NetworkHandler
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.Retrofit.Builder
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


/**
 * Created by Joás V. Pereira
 */

@Module
open class NetworkModule(internal var baseUrl:String,
                    internal var cacheSize:Long) {

    //OkHttpClient
    @Provides
    @Singleton
    fun provideOkHttpClient(application: CodeChallangeAplication,cache: Cache): OkHttpClient {

        val interceptor = Interceptor { chain ->
            val originalRequest = chain.request()
            val cacheHeaderValue = if (application.isConnected())
                "public, max-age=2419200"
            else
                "public, only-if-cached, max-stale=2419200"
            val request = originalRequest.newBuilder().build()
            val response = chain.proceed(request)
            response.newBuilder()
                    .removeHeader("Pragma")
                    .removeHeader("Cache-Control")
                    .header("Cache-Control", cacheHeaderValue)
                    .build()
        }

        val builder = OkHttpClient.Builder()
        builder
                .cache(cache)
                .addInterceptor(interceptor)
                .addNetworkInterceptor(interceptor)
        return builder.build()
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return GsonBuilder()
                //.registerTypeAdapter(Languages::class.java, LanguageDeserializer())
                .create()
    }

    @Provides
    @Singleton
    fun provideEndpoint(): String {
        return baseUrl
    }

    @Provides
    @Singleton
    fun provideRetrofit(
            endpoint: String,
            okHttpClient: OkHttpClient,
            gson: Gson): Retrofit {
        val builder = Builder()
        builder
                .baseUrl(endpoint)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        return builder.build()
    }

    @Provides
    @Singleton
    fun providesNetworkHandler(re: Retrofit): NetworkHandler {
        return NetworkHandler(re)
    }



    @Provides
    @Singleton
    fun provideOkHttpCache(application: CodeChallangeAplication): Cache {
        return Cache(application.cacheDir, cacheSize)
    }
}
