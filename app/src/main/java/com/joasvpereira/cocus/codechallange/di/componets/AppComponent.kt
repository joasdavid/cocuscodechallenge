package com.joasvpereira.cocus.codechallange.di.componets

import com.joasvpereira.cocus.codechallange.ui.MainActivity
import com.joasvpereira.cocus.codechallange.di.modules.*
import com.joasvpereira.cocus.codechallange.ui.challenge.AuthoredChallengesFragment
import com.joasvpereira.cocus.codechallange.ui.challenge.CompletedChallengesFragment
import com.joasvpereira.cocus.codechallange.ui.challenge.details.CodeChallengeFragment
import com.joasvpereira.cocus.codechallange.ui.user.SearchUserFragment
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Joás V. Pereira
 */
@Singleton
@Component(
        modules = arrayOf(
                AppModule::class,
                NetworkModule::class,
                DataBaseModule::class,
                RepositoryModule::class,
                ViewModelsModule::class
        )
)
interface AppComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(fragment: SearchUserFragment)
    fun inject(fragment: CompletedChallengesFragment)
    fun inject(fragment: AuthoredChallengesFragment)
    fun inject(fragment: CodeChallengeFragment)
}

