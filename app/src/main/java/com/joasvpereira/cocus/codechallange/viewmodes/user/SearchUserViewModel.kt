package com.joasvpereira.cocus.codechallange.viewmodes.user

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.joasvpereira.cocus.codechallange.logic.network.results.UserResult
import com.joasvpereira.cocus.codechallange.logic.repository.CallbackInterface
import com.joasvpereira.cocus.codechallange.logic.repository.RepositoryResult
import com.joasvpereira.cocus.codechallange.logic.repository.users.UserDataAccess
import com.joasvpereira.cocus.codechallange.viewmodes.ScreenBaseViewModel
import javax.inject.Inject

/**
 * Created by Joás V. Pereira
 * on 06 Feb. 2019.
 */
class SearchUserViewModel
@Inject constructor(private val repository: UserDataAccess) : ScreenBaseViewModel() {

    private var isOrderByRank = false
    private val userList: LiveData<List<UserResult>> = MutableLiveData()

    fun searchUser(name: String, observer: Observer<UserResult>? = null) {
        isLoading.value = true
        repository.getUserByName(name, object : CallbackInterface<UserResult> {
            override fun onResult(data: RepositoryResult<UserResult>) {
                isLoading.value = false
                if (data.isSuccessState) {
                    fetchListOfUsers(isOrderByRank)
                    observer?.onChanged(data.dataResult)
                }
            }
        })
    }

    fun fetchListOfUsers(isOrderByRank: Boolean): LiveData<List<UserResult>> {
        isLoading.value = true
        this.isOrderByRank = isOrderByRank
        repository.getUserList(isOrderByRank, object : CallbackInterface<List<UserResult>> {
            override fun onResult(data: RepositoryResult<List<UserResult>>) {
                isLoading.value = false
                if (data.isSuccessState) {
                    (userList as MutableLiveData).value = data.dataResult
                } else {
                    //failed case!!
                }
            }

        })
        return userList
    }

}
