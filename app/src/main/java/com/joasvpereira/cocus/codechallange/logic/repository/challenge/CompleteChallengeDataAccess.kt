package com.joasvpereira.cocus.codechallange.logic.repository.challenge

import com.joasvpereira.cocus.codechallange.logic.network.results.CompletedChallengesResult
import com.joasvpereira.cocus.codechallange.logic.network.results.UserResult
import com.joasvpereira.cocus.codechallange.logic.repository.CallbackInterface

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
interface CompleteChallengeDataAccess {
    fun getCompletedChallenge(
            name: String,
            page: Int,
            callback: CallbackInterface<CompletedChallengesResult>)
}