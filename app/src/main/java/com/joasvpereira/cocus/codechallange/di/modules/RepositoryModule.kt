package com.joasvpereira.cocus.codechallange.di.modules

import com.google.gson.Gson
import com.joasvpereira.cocus.codechallange.db.AppDatabase
import com.joasvpereira.cocus.codechallange.logic.network.NetworkHandler
import com.joasvpereira.cocus.codechallange.logic.repository.challenge.AuthoredChallengeCodeWars
import com.joasvpereira.cocus.codechallange.logic.repository.challenge.AuthoredChallengeDataAccess
import com.joasvpereira.cocus.codechallange.logic.repository.challenge.CompleteChallengeCodeWars
import com.joasvpereira.cocus.codechallange.logic.repository.challenge.CompleteChallengeDataAccess
import com.joasvpereira.cocus.codechallange.logic.repository.challenge.details.CodeChallengeCodeWars
import com.joasvpereira.cocus.codechallange.logic.repository.challenge.details.CodeChallengeDataAccess
import com.joasvpereira.cocus.codechallange.logic.repository.users.UserDataAccess
import com.joasvpereira.cocus.codechallange.logic.repository.users.UserCodeWars
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Joás V. Pereira
 * on 06 Feb. 2019.
 */
@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providesUserDataAccess(
            networkHandler: NetworkHandler,
            gson: Gson,
            database: AppDatabase): UserDataAccess {
        return UserCodeWars(networkHandler, gson,database)
    }

    @Provides
    @Singleton
    fun providesCompleteChallengeDataAccess(networkHandler: NetworkHandler): CompleteChallengeDataAccess {
        return CompleteChallengeCodeWars(networkHandler)
    }

    @Provides
    @Singleton
    fun providesAuthoredChallengeDataAccess(networkHandler: NetworkHandler): AuthoredChallengeDataAccess {
        return AuthoredChallengeCodeWars(networkHandler)
    }

    @Provides
    @Singleton
    fun providesCodeChallengeDataAccess(networkHandler: NetworkHandler): CodeChallengeDataAccess {
        return CodeChallengeCodeWars(networkHandler)
    }
}
