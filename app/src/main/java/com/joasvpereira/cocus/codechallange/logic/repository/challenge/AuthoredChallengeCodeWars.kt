package com.joasvpereira.cocus.codechallange.logic.repository.challenge

import com.joasvpereira.cocus.codechallange.logic.network.NetworkHandler
import com.joasvpereira.cocus.codechallange.logic.network.results.AuthoredChallengesResult
import com.joasvpereira.cocus.codechallange.logic.repository.CallbackInterface
import com.joasvpereira.cocus.codechallange.logic.repository.RepositoryResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */

class AuthoredChallengeCodeWars
@Inject constructor(val networkHandler: NetworkHandler) : AuthoredChallengeDataAccess {

    override fun getAuthoredChallenges(user: String, callback: CallbackInterface<AuthoredChallengesResult>) {
        val observable =
                networkHandler.getService()
                        .getUserAuthoredChallenges(user)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
        observable.subscribeWith(getAuthoredChallengesDataObserver(callback))
    }

    private fun getAuthoredChallengesDataObserver(
            callback: CallbackInterface<AuthoredChallengesResult>
    ): DisposableObserver<Response<AuthoredChallengesResult>> {
        val observer: DisposableObserver<Response<AuthoredChallengesResult>>
                = object : DisposableObserver<Response<AuthoredChallengesResult>>() {

            override fun onNext(@io.reactivex.annotations.NonNull userResponse: Response<AuthoredChallengesResult>) {
                if (userResponse.isSuccessful && userResponse.code() == 200) {
                    userResponse.body()?.let {
                        callback.onResult(RepositoryResult(
                                isSuccessState = true,
                                dataResult = it
                        ))
                    }
                } else {
                    callback.onResult(RepositoryResult(message =
                    "Error (" + userResponse.code() + ") - "
                            + userResponse.message())
                    )
                }
            }


            override fun onError(@io.reactivex.annotations.NonNull e: Throwable) {
                callback.onResult(RepositoryResult(message = e.message + ""))
            }

            override fun onComplete() {

            }
        }

        return observer
    }
}


