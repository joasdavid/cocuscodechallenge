package com.joasvpereira.cocus.codechallange.logic.repository.users

import com.google.gson.Gson
import com.joasvpereira.cocus.codechallange.db.AppDatabase
import com.joasvpereira.cocus.codechallange.db.entities.UserEntry
import com.joasvpereira.cocus.codechallange.logic.network.NetworkHandler
import com.joasvpereira.cocus.codechallange.logic.network.results.UserResult
import com.joasvpereira.cocus.codechallange.logic.repository.CallbackInterface
import com.joasvpereira.cocus.codechallange.logic.repository.RepositoryResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by Joás V. Pereira
 * on 28 Jan. 2019.
 */
class UserCodeWars
@Inject constructor(
        val networkHandler: NetworkHandler,
        val gson: Gson,
        val appDatabase: AppDatabase
) : UserDataAccess {

    override fun getUserByName(name: String, callback: CallbackInterface<UserResult>?) {
        val userResultObservable =
                networkHandler
                        .getService()
                        .searchUserByName(name)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
        userResultObservable.subscribeWith(getUserNameDataObserver(callback))
    }

    private fun getUserNameDataObserver(userCallback: CallbackInterface<UserResult>?): DisposableObserver<Response<UserResult>> {
        val observer: DisposableObserver<Response<UserResult>>
                = object : DisposableObserver<Response<UserResult>>() {

            override fun onNext(@io.reactivex.annotations.NonNull userResponse: Response<UserResult>) {
                if (userResponse.isSuccessful && userResponse.code() == 200) {
                    val userDb = UserEntry(
                            userResponse.body()!!.username,
                            gson.toJson(userResponse.body()),
                            userResponse.body()!!.leaderboardPosition
                    )
                    appDatabase.userDao().insertUser(userDb)

                    userResponse.body()?.let {
                        val result: RepositoryResult<UserResult> = RepositoryResult()
                        userCallback?.onResult(RepositoryResult(
                                isSuccessState = true,
                                dataResult = it
                        ))
                    }
                } else {
                    userCallback?.onResult(RepositoryResult(message =
                    "Error (" + userResponse.code() + ") - "
                            + userResponse.message())
                    )
                }
            }


            override fun onError(@io.reactivex.annotations.NonNull e: Throwable) {
                userCallback?.onResult(RepositoryResult(message = e.message + ""))
            }

            override fun onComplete() {

            }
        }
        return observer
    }

    override fun getUserList(isByRank: Boolean, callback: CallbackInterface<List<UserResult>>) {
        val sendList = ArrayList<UserResult>()

        retrieveUserList(isByRank, sendList)

        callback.onResult(
                RepositoryResult<List<UserResult>>(
                        isSuccessState = !sendList.isEmpty(),
                        dataResult = sendList
                )
        )
    }

    fun retrieveUserList(isByRank: Boolean, sendList: ArrayList<UserResult>) {
        val userEntries: List<UserEntry>
        if (isByRank)
            userEntries =
                    appDatabase.userDao().getUserByRank()
        else
            userEntries =
                    appDatabase.userDao().getUsersById()


        for (user in userEntries) {
            sendList.add(
                    gson.getAdapter(UserResult::class.java).fromJson(user.userResultJson)
            )
        }
    }
}
