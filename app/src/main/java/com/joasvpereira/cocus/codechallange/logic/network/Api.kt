package com.joasvpereira.cocus.codechallange.logic.network

import com.joasvpereira.cocus.codechallange.logic.network.results.AuthoredChallengesResult
import com.joasvpereira.cocus.codechallange.logic.network.results.CodeChallengeResult
import com.joasvpereira.cocus.codechallange.logic.network.results.CompletedChallengesResult
import com.joasvpereira.cocus.codechallange.logic.network.results.UserResult
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Joás V. Pereira
 * on 28 Jan. 2019.
 */

interface Api {

    /**
     * GET interface to search for a user by name, returning the information about the same.
     * @param name name of the user.
     * @return UserResult with all user information.
     */
    @GET("users/{userName}")
    fun searchUserByName(
            @Path("userName") name: String
    ): Observable<Response<UserResult>>

    /**
     * GET interface to retrieve completed challenges of a specific user.
     * @param name - Name of the user with completed challenges.
     * @param pageNumber - Target page of completed challenges.
     * @return CompletedChallengesResult with a list of completed challenges and paging information.
     */
    @GET("users/{userName}/code-challenges/completed")
    fun getUserCompletedChallenges(
            @Path("userName") name: String,
            @Query("page") pageNumber : Int
    ): Observable<Response<CompletedChallengesResult>>

    /**
     * GET interface to retrieve completed authored of a specific user.
     * @param name - Name of the user with authored challenges.
     * @return CompletedChallengesResult with a list of authored challenges.
     */
    @GET("users/{userName}/code-challenges/authored")
    fun getUserAuthoredChallenges(
            @Path("userName") name: String
    ):Observable<Response<AuthoredChallengesResult>>


    /**
     * GET interface to provide a information about one challenge.
     * @param codeChallengeId - id of a challenge.
     * @return all challenge information.
     */
    @GET("code-challenges/{codeChallengeId}")
    fun getCodeChallenge(
            @Path("codeChallengeId") codeChallengeId: String
    ): Observable<Response<CodeChallengeResult>>
}
