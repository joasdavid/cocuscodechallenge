package com.joasvpereira.cocus.codechallange.logic.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.Languages
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Joás V. Pereira
 * on 28 Jan. 2019.
 */

class test() {


    var isConnected: Boolean = true

    val retrofit: Retrofit
        get() = Retrofit.Builder()
                .baseUrl(endPoint)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

    val okHttpClient: OkHttpClient
        get() {

            return okHttpClientBuilder.build()
        }

    val okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
    /**
    get() {
    val builder = OkHttpClient.Builder()
    return addInterceptors(
    addNetworkInterceptors(
    builder
    )
    )
    }
     */

    val gson: Gson
    get() = GsonBuilder()
            //.registerTypeAdapter(Languages::class.java, LanguagesDeserializer())
            .create()


    val interceptor: Interceptor = Interceptor { chain ->
        val originalRequest = chain.request()
        val cacheHeaderValue = if (isConnected)
            "public, max-age=2419200"
        else
            "public, only-if-cached, max-stale=2419200"
        val request = originalRequest.newBuilder().build()
        val response = chain.proceed(request)
        response.newBuilder()
                .removeHeader("Pragma")
                .removeHeader("Cache-Control")
                .header("Cache-Control", cacheHeaderValue)
                .build()
    }

    val endPoint: String = "http://www.codewars.com/api/v1/"

    fun addInterceptors(builder: OkHttpClient.Builder): OkHttpClient.Builder {
        builder.addInterceptor(interceptor)
        return builder
    }

    fun addNetworkInterceptors(builder: OkHttpClient.Builder): OkHttpClient.Builder {
        builder.addNetworkInterceptor(interceptor)
        return builder
    }
}
