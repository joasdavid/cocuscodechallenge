package com.joasvpereira.cocus.codechallange.logic.network.results

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.Rank
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.Unresolved
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.UserReference

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */

data class CodeChallengeResult(@Expose
                               @SerializedName("unresolved")
                               var unresolved: Unresolved? = null,
                               @Expose
                               @SerializedName("contributorsWanted")
                               var contributorsWanted: Boolean = false,
                               @Expose
                               @SerializedName("tags")
                               var tags: List<String>? = null,
                               @Expose
                               @SerializedName("voteScore")
                               var voteScore: Int = 0,
                               @Expose
                               @SerializedName("totalStars")
                               var totalStars: Int = 0,
                               @Expose
                               @SerializedName("totalCompleted")
                               var totalCompleted: Int = 0,
                               @Expose
                               @SerializedName("totalAttempts")
                               var totalAttempts: Int = 0,
                               @Expose
                               @SerializedName("description")
                               var description: String? = null,
                               @Expose
                               @SerializedName("approvedBy")
                               var approvedBy: UserReference? = null,
                               @Expose
                               @SerializedName("createdBy")
                               var createdBy: UserReference? = null,
                               @Expose
                               @SerializedName("createdAt")
                               var createdAt: String? = null,
                               @Expose
                               @SerializedName("rank")
                               var rank: Rank? = null,
                               @Expose
                               @SerializedName("url")
                               var url: String? = null,
                               @Expose
                               @SerializedName("languages")
                               var languages: List<String>? = null,
                               @Expose
                               @SerializedName("approvedAt")
                               var approvedAt: String? = null,
                               @Expose
                               @SerializedName("publishedAt")
                               var publishedAt: String? = null,
                               @Expose
                               @SerializedName("category")
                               var category: String? = null,
                               @Expose
                               @SerializedName("slug")
                               var slug: String? = null,
                               @Expose
                               @SerializedName("name")
                               var name: String? = null,
                               @Expose
                               @SerializedName("id")
                               var id: String? = null)
