package com.joasvpereira.cocus.codechallange.viewmodes.challenge.completed

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.joasvpereira.cocus.codechallange.logic.network.results.CompletedChallengesResult
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.CompletedChallenge
import com.joasvpereira.cocus.codechallange.logic.repository.CallbackInterface
import com.joasvpereira.cocus.codechallange.logic.repository.RepositoryResult
import com.joasvpereira.cocus.codechallange.logic.repository.challenge.CompleteChallengeDataAccess
import com.joasvpereira.cocus.codechallange.viewmodes.ScreenBaseViewModel
import javax.inject.Inject
import android.graphics.Bitmap



/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
class CompletedChallengesViewModel
@Inject constructor(private val repository: CompleteChallengeDataAccess) : ScreenBaseViewModel() {

    companion object {
        val LOAD_END = "ALL_PAGES_LOADED"
    }

    private val completedChallengesList = MutableLiveData<List<CompletedChallenge>>()
    private var lastUser = ""
    private var currentPage = 0
    private var totalPages = 0

    fun listOfCompletedChallenges(user: String, pag: Int = currentPage, withLoading: Boolean = true): LiveData<List<CompletedChallenge>> {
        //loadList()
        lastUser = user
        isLoading.value = withLoading
        repository.getCompletedChallenge(
                user,
                pag,
                object : CallbackInterface<CompletedChallengesResult> {
                    override fun onResult(data: RepositoryResult<CompletedChallengesResult>) {
                        isLoading.value = false
                        if (data.isSuccessState){
                            completedChallengesList.value = data.dataResult?.CompletedChallenge
                            totalPages = data.dataResult?.totalPages ?: 0
                            if(!haveMorePages()){messageData.value = LOAD_END}
                        }else{
                            messageData.value = data.message
                        }
                    }

                }
        )
        return completedChallengesList
    }

    @Suppress("unused")
    fun loadList() {
        val list = ArrayList<CompletedChallenge>()
        for (i in 0..2) {
            list.add(
                    CompletedChallenge(
                            id = "$i",
                            name = "name$i",
                            slug = "slug$i",
                            completedAt = "0$i-05-2019"
                    ))
        }
        completedChallengesList.value = list
    }

    fun nextPage() {
        if (haveMorePages()) {
            listOfCompletedChallenges(
                    lastUser,
                    ++currentPage,
                    false
            )
        }
    }

    private fun haveMorePages(): Boolean {
        return currentPage < totalPages - 1
    }

}