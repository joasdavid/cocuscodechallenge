package com.joasvpereira.cocus.codechallange.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.joasvpereira.cocus.codechallange.db.dao.UserDao
import com.joasvpereira.cocus.codechallange.db.entities.UserEntry

/**
 * Created by Joás V. Pereira
 * on 06 Feb. 2019.
 */
@Database(entities = arrayOf(UserEntry::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}