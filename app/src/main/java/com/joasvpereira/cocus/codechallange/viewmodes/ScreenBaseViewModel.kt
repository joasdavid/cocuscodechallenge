package com.joasvpereira.cocus.codechallange.viewmodes

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

/**
 * Created by Joás V. Pereira
 * on 06 Feb. 2019.
 */
open class ScreenBaseViewModel(): ViewModel() {

    protected var isLoading: MutableLiveData<Boolean> = MutableLiveData()
    protected var messageData: MutableLiveData<String> = MutableLiveData()

    init {
        isLoading.value = false
    }

    fun loadingState():LiveData<Boolean>{
        return isLoading
    }

    fun vmMessages():LiveData<String>{
        return messageData
    }

}