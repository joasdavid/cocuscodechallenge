package com.joasvpereira.cocus.codechallange.logic.network.results.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
data class UserReference(@Expose
                         @SerializedName("url")
                         val url: String,
                         @Expose
                         @SerializedName("username")
                         val username: String)