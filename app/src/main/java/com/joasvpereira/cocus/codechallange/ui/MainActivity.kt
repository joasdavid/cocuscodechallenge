package com.joasvpereira.cocus.codechallange.ui

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.joasvpereira.cocus.codechallange.R
import com.joasvpereira.cocus.codechallange.logic.network.results.UserResult
import com.joasvpereira.cocus.codechallange.ui.user.SearchUserFragment
import com.joasvpereira.cocus.codechallange.viewmodes.user.SearchUserViewModel
import kotlinx.android.synthetic.main.activity_base.*
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        replaceFragment(baseFragmentContainer,
                SearchUserFragment(),
                supportFragmentManager,
                false
        )
    }
}
