package com.joasvpereira.cocus.codechallange;

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */

public class NetworkConfigs {

  public static final String END_POINT = "http://www.codewars.com/api/v1/";
  public static final long CACHE_SIZE = 10 * 1000 * 1000;

}
