package com.joasvpereira.cocus.codechallange.logic.network.results

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.CodeChallenges
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.Ranks

/**
 * Created by Joás V. Pereira
 * on 15 Aug. 2018.
 */

data class UserResult(@Expose
                 @SerializedName("codeChallenges")
                 var codeChallenges: CodeChallenges? = null,
                 @Expose
                 @SerializedName("ranks")
                 var ranks: Ranks? = null,
                 @Expose
                 @SerializedName("skills")
                 var skills: List<String>? = null,
                 @Expose
                 @SerializedName("leaderboardPosition")
                 var leaderboardPosition: Int = 0,
                 @Expose
                 @SerializedName("clan")
                 var clan: String? = null,
                 @Expose
                 @SerializedName("honor")
                 var honor: Int = 0,
                 @Expose
                 @SerializedName("name")
                 var name: String? = null,
                 @Expose
                 @SerializedName("username")
                 var username: String)
