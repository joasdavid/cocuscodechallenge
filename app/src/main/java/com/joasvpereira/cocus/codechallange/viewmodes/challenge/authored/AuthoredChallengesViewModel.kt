package com.joasvpereira.cocus.codechallange.viewmodes.challenge.authored

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.joasvpereira.cocus.codechallange.logic.network.results.AuthoredChallengesResult
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.AuthoredChallenge
import com.joasvpereira.cocus.codechallange.logic.repository.CallbackInterface
import com.joasvpereira.cocus.codechallange.logic.repository.RepositoryResult
import com.joasvpereira.cocus.codechallange.logic.repository.challenge.AuthoredChallengeDataAccess
import com.joasvpereira.cocus.codechallange.viewmodes.ScreenBaseViewModel
import javax.inject.Inject

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
class AuthoredChallengesViewModel
@Inject constructor(private val repository: AuthoredChallengeDataAccess) : ScreenBaseViewModel(){

    private val authoredChallengesList = MutableLiveData<List<AuthoredChallenge>>()
    private var lastUser: String = ""

    fun listOfAuthoredChallenges(user: String): LiveData<List<AuthoredChallenge>>{
        isLoading.value = true
        lastUser = user
        repository.getAuthoredChallenges(
                user,
                object: CallbackInterface<AuthoredChallengesResult>{
                    override fun onResult(data: RepositoryResult<AuthoredChallengesResult>) {
                        isLoading.value = false
                        if (data.isSuccessState){
                            authoredChallengesList.value = data.dataResult?.authoredChallenges
                        }else{
                            messageData.value = data.message
                        }
                    }
                })

        return authoredChallengesList
    }
}