package com.joasvpereira.cocus.codechallange.logic.network

import retrofit2.Retrofit
import javax.inject.Inject

/**
 * Created by Joás V. Pereira
 * on 28 Jan. 2019.
 */
class NetworkHandler @Inject constructor(private val retrofit: Retrofit) {

    fun getService(): Api {
        return retrofit.create(Api::class.java)
    }

}