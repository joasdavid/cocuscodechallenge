package com.joasvpereira.cocus.codechallange.logic.repository.challenge

import com.joasvpereira.cocus.codechallange.db.entities.UserEntry
import com.joasvpereira.cocus.codechallange.logic.network.NetworkHandler
import com.joasvpereira.cocus.codechallange.logic.network.results.CompletedChallengesResult
import com.joasvpereira.cocus.codechallange.logic.network.results.UserResult
import com.joasvpereira.cocus.codechallange.logic.repository.CallbackInterface
import com.joasvpereira.cocus.codechallange.logic.repository.RepositoryResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */

class CompleteChallengeCodeWars
@Inject constructor(val networkHandler: NetworkHandler) : CompleteChallengeDataAccess {

    override fun getCompletedChallenge(
            name: String,
            page: Int,
            callback: CallbackInterface<CompletedChallengesResult>) {
        val observable =
        networkHandler.getService()
                .getUserCompletedChallenges(name,page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        observable.subscribeWith(getCompleteChallengesDataObserver(callback))
    }

    private fun getCompleteChallengesDataObserver(
            userCallback: CallbackInterface<CompletedChallengesResult>?
    ): DisposableObserver<Response<CompletedChallengesResult>> {
        val observer: DisposableObserver<Response<CompletedChallengesResult>>
                = object : DisposableObserver<Response<CompletedChallengesResult>>() {

            override fun onNext(@io.reactivex.annotations.NonNull userResponse: Response<CompletedChallengesResult>) {
                if (userResponse.isSuccessful && userResponse.code() == 200) {
                    userResponse.body()?.let {
                        userCallback?.onResult(RepositoryResult(
                                isSuccessState = true,
                                dataResult = it
                        ))
                    }
                } else {
                    userCallback?.onResult(RepositoryResult(message =
                    "Error (" + userResponse.code() + ") - "
                            + userResponse.message())
                    )
                }
            }


            override fun onError(@io.reactivex.annotations.NonNull e: Throwable) {
                userCallback?.onResult(RepositoryResult(message = e.message + ""))
            }

            override fun onComplete() {

            }
        }
        return observer
    }
}
