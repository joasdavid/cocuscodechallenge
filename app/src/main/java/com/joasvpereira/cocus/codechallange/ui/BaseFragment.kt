package com.joasvpereira.cocus.codechallange.ui

import android.support.v4.app.Fragment
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.search_user_fragment.*

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
abstract class BaseFragment : Fragment(){

    override fun onResume() {
        super.onResume()
        activity?.setTitle(pageTitle())
    }

    abstract fun pageTitle():String

    fun getBaseActivity():BaseActivity{
        return activity as BaseActivity
    }

    protected fun showMessages(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}