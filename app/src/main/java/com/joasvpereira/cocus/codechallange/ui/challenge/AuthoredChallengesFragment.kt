package com.joasvpereira.cocus.codechallange.ui.challenge

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.joasvpereira.cocus.codechallange.R
import com.joasvpereira.cocus.codechallange.constants.ShareDataConstants
import com.joasvpereira.cocus.codechallange.ui.adapters.AuthoredChallengeAdapter
import com.joasvpereira.cocus.codechallange.ui.adapters.holders.AdapterItemInterface
import com.joasvpereira.cocus.codechallange.viewmodes.challenge.authored.AuthoredChallengesViewModel
import kotlinx.android.synthetic.main.completed_challenges_fragment.*
import javax.inject.Inject

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */

class AuthoredChallengesFragment: BaseChallengesFragment(){

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    lateinit var viewModel: AuthoredChallengesViewModel
    private lateinit var adapter: AuthoredChallengeAdapter

    private var user = ""

    override fun pageTitle(): String {
        return user
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.completed_challenges_fragment,
                container,
                false
        )
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (getBaseActivity().screensShareData.containsKey(ShareDataConstants.USER)) {
            user = getBaseActivity().screensShareData.get(ShareDataConstants.USER).toString()
        }

        getBaseActivity().appComponent.inject(this)
        viewModel = ViewModelProviders.of(this, factory)
                .get(AuthoredChallengesViewModel::class.java)
        initUI()
        initObserves()

    }

    fun initUI() {
        completedChallengesRv.setLayoutManager(LinearLayoutManager(context))
        //noinspection ConstantConditions
        completedChallengesRv.addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL))

        adapter = AuthoredChallengeAdapter(object : AdapterItemInterface {
            override fun itemClicked(view: View, pos: Int) {
                val id = adapter.list.get(pos).id
                openDetailsActivity(id)
            }
        })

        completedChallengesRv.setAdapter(adapter)
    }

    fun initObserves() {
        viewModel.loadingState().observe(this, Observer {
            it.let { loading(it) }
        })

        viewModel.vmMessages().observe(this, Observer {
            it?.let {
                showMessages(it)
            }
        })

        viewModel.listOfAuthoredChallenges(user).observe(this, Observer {
            adapter.addAuthoredChallenges(it)
            adapter.notifyDataSetChanged()
        })
    }

    private fun loading(boolean: Boolean?) {
        if (boolean == true)
            loadingView.visibility = View.VISIBLE
        else
            loadingView.visibility = View.GONE
    }

}
