package com.joasvpereira.cocus.codechallange.logic.network.results

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.CompletedChallenge

/**
 * Created by Joás V. Pereira
 * on 28 Jan. 2019.
 */
data class CompletedChallengesResult(
        @Expose
        @SerializedName("data")
        var CompletedChallenge: List<CompletedChallenge> = ArrayList(),
        @Expose
        @SerializedName("totalItems")
        var totalItems: Int = 0,
        @Expose
        @SerializedName("totalPages")
        var totalPages: Int = 0
)