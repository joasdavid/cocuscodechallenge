package com.joasvpereira.cocus.codechallange.di.modules

import android.app.Application
import com.joasvpereira.cocus.codechallange.CodeChallangeAplication
import dagger.Module
import javax.inject.Singleton
import dagger.Provides



/**
 * Created by Joás V. Pereira
 * on 06 Feb. 2019.
 */
@Module
class AppModule(internal var mApplication: CodeChallangeAplication) {

    @Provides
    @Singleton
    internal fun providesApplication(): CodeChallangeAplication {
        return mApplication
    }
}