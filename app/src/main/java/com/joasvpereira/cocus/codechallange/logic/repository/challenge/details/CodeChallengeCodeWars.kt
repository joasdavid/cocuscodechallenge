package com.joasvpereira.cocus.codechallange.logic.repository.challenge.details

import com.joasvpereira.cocus.codechallange.logic.network.NetworkHandler
import com.joasvpereira.cocus.codechallange.logic.network.results.CodeChallengeResult
import com.joasvpereira.cocus.codechallange.logic.repository.CallbackInterface
import com.joasvpereira.cocus.codechallange.logic.repository.RepositoryResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
class CodeChallengeCodeWars
@Inject constructor(val networkHandler: NetworkHandler) :CodeChallengeDataAccess{

    override fun getCodeChallenges(code: String, callback: CallbackInterface<CodeChallengeResult>) {
        val observable =
                networkHandler.getService()
                        .getCodeChallenge(code)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
        observable.subscribeWith(object : DisposableObserver<Response<CodeChallengeResult>>(){
            override fun onNext(response: Response<CodeChallengeResult>) {
                if (response.isSuccessful && response.code() == 200) {
                    response.body()?.let {
                        callback.onResult(RepositoryResult(
                                isSuccessState = true,
                                dataResult = it
                        ))
                    }
                } else {
                    callback.onResult(RepositoryResult(message =
                    "Error (" + response.code() + ") - "
                            + response.message())
                    )
                }
            }

            override fun onError(@io.reactivex.annotations.NonNull e: Throwable) {
                callback.onResult(RepositoryResult(message = e.message + ""))
            }

            override fun onComplete() {

            }
        }
        )
    }

}