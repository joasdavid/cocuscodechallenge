package com.joasvpereira.cocus.codechallange.viewmodes.challenge.details

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.joasvpereira.cocus.codechallange.logic.network.results.CodeChallengeResult
import com.joasvpereira.cocus.codechallange.logic.network.results.CompletedChallengesResult
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.CompletedChallenge
import com.joasvpereira.cocus.codechallange.logic.repository.CallbackInterface
import com.joasvpereira.cocus.codechallange.logic.repository.RepositoryResult
import com.joasvpereira.cocus.codechallange.logic.repository.challenge.CompleteChallengeDataAccess
import com.joasvpereira.cocus.codechallange.logic.repository.challenge.details.CodeChallengeDataAccess
import com.joasvpereira.cocus.codechallange.viewmodes.ScreenBaseViewModel
import javax.inject.Inject

/**
 * Created by Joás V. Pereira
 * on 10 Feb. 2019.
 */
class CodeChallengesViewModel
@Inject constructor(private val repository: CodeChallengeDataAccess) : ScreenBaseViewModel() {

    private val codeChallengesList = MutableLiveData<CodeChallengeResult>()


    fun codeChallengeDetails(id: String): LiveData<CodeChallengeResult> {
        isLoading.value = true
        repository.getCodeChallenges(
                id,
                object : CallbackInterface<CodeChallengeResult> {
                    override fun onResult(data: RepositoryResult<CodeChallengeResult>) {
                        isLoading.value = false
                        if (data.isSuccessState){
                            codeChallengesList.value = data.dataResult
                        }else{
                            messageData.value = "err"
                        }
                    }

                }
        )
        return codeChallengesList
    }

}