package com.joasvpereira.cocus.codechallange.ui.adapters;

/**
 * Created by Joás V. Pereira
 * on 21 Aug. 2018.
 */

public interface OnLoadMoreListener {
  void onLoadMore();
}