package com.joasvpereira.cocus.codechallange;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeThat;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import com.joasvpereira.cocus.codechallange.di.componets.AppComponent;
import com.joasvpereira.cocus.codechallange.di.modules.NetworkModule;
import com.joasvpereira.cocus.codechallange.logic.network.NetworkHandler;
import com.joasvpereira.cocus.codechallange.logic.network.results.AuthoredChallengesResult;
import com.joasvpereira.cocus.codechallange.logic.network.results.CodeChallengeResult;
import com.joasvpereira.cocus.codechallange.logic.network.results.CompletedChallengesResult;
import com.joasvpereira.cocus.codechallange.logic.network.results.UserResult;
import com.joasvpereira.cocus.codechallange.logic.network.results.objects.Ranks;
import com.joasvpereira.cocus.codechallange.logic.network.test;
import dagger.Component;
import javax.inject.Singleton;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import retrofit2.Response;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

  test tt;
  NetworkHandler networkHandler;

  @Before
  public void init() {
    tt = new test();
    networkHandler = new NetworkHandler(tt.getRetrofit());
  }

  @Test
  public void useAppContext() throws Exception {
    // Context of the app under test.
    Context appContext = InstrumentationRegistry.getTargetContext();

    assertEquals("com.joasvpereira.cocus.codechallange", appContext.getPackageName());

  }

  @Test
  public void getUserInformationTest() {
    Response<UserResult> response =
        new NetworkHandler(tt.getRetrofit()).getService().searchUserByName("cliffstamp")
            .blockingFirst();
    assumeThat(response.isSuccessful(), is(true));
    assertEquals(response.body().getUsername(), "cliffstamp");
  }

  @Test
  public void getUserLanguageCountTest() {
    Response<UserResult> response =
        new NetworkHandler(tt.getRetrofit()).getService().searchUserByName("cliffstamp")
            .blockingFirst();
    assumeThat(response.isSuccessful(), is(true));
    Ranks das = response.body()
        .getRanks();
    int size = das.getLanguages().size();
    assertEquals(size, 20);
  }

  @Test
  public void getUserInformation404Test() {
    Response<UserResult> response =
        networkHandler.getService().searchUserByName("!#@£").blockingFirst();
    assertTrue(response != null);
  }

  @Test
  public void getUserCompletedChallengesTest() {
    Response<CompletedChallengesResult> response =
        networkHandler.getService().getUserCompletedChallenges(
            "cliffstamp",
            0
        ).blockingFirst();
    assumeThat(response.isSuccessful(), is(true));
  }

  @Test
  public void getUserAuthoredChallengesTest() {
    Response<AuthoredChallengesResult> response =
        networkHandler.getService().getUserAuthoredChallenges("cliffstamp").blockingFirst();
    assumeThat(response.isSuccessful(), is(true));
  }

  @Test
  public void getUserAuthoredChallengesTest2() {
    Response<AuthoredChallengesResult> response =
        networkHandler.getService().getUserAuthoredChallenges("cliffstamp").blockingFirst();
    assumeThat(response.isSuccessful(), is(true));
  }

  @Test
  public void getCodeChallengesTest() {
    Response<CodeChallengeResult> response =
        networkHandler.getService().getCodeChallenge("573992c724fc289553000e95").blockingFirst();
    assertNotNull(response.body());
  }

}
